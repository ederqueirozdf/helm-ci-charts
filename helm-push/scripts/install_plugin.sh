#!/bin/sh -e

version="$(cat plugin.yaml | grep "version" | cut -d '"' -f 2)"
echo "Downloading and installing helm-push v${version} ..."

mkdir -p "bin"
mkdir -p "releases/v${version}"

# Download with curl if possible.
DOWNLOAD_FILE=$(find /app -name "*helm-push*.gz")
tar xf "$DOWNLOAD_FILE" -C "/app/releases"
cp -rf "/app/releases/bin/helm-cm-push" "/usr/local/bin/"
mv "/app/releases/bin/helm-cm-push" "bin/helm-cm-push"

#version
helm cm-push --help

