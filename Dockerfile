FROM python:3.7-alpine

ENV HELM_REPO_USE_HTTP="true"
ENV HELM_EXPERIMENTAL_OCI="1"

WORKDIR /app

COPY . .

RUN apk add curl openssl bash git jq --no-cache

# INSTALL PLUGIN HELM v3
RUN tar zxf releases/helm-v3.7.2-linux-amd64.tar.gz -C releases/ \
	&& mv releases/linux-amd64/helm /usr/local/bin/helm 
# INSTALL PLUGIN HELM PUSH
RUN helm plugin install /app/helm-push \
# INSTALL PLUGIN HELM UNITTEST
    && helm plugin install /app/helm-unittest

ENTRYPOINT ["helm"]
CMD ["version"]


